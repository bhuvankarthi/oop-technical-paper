# Java 
   Java is an Object Oriented Programming language .

## Class and Objects

   Everything in Java is associated with Classes and objects , as Classes are the blue print of Objects and Objects are the behaviour of Classes .

   ### Object 
    * Object is a collection of data .
    * objects knows something and does something .
    * It does something using methods .

   ### Classes
    * Behaviour of objects is based on class . 
    * The methods that objects use are defined inside the class.

## Fundamental OOP's concepts of java 
    Java supports some concepts that can we worked or included with Classes and Objects .
    
    * Encapsulation
    * Inheritance
    * Polymorphism
    * Abstraction 


## Encapsulation 
    * It is a method of binding the data with methods .

    * The variables that are declared inside the class can be made as private .

    * The data inside the class will be secured by using Private , with this the Class outside the particular class cannot directly access the variable .

    * Those data can be accessed by using methods that is created inside the class and that method could be public .

    * This can be achieved by getter and setter method .

### Encapsulation Code Sample 

```
class Student {
    private int rollNo ;
    private String name ;
    private int marks ;

    public void setRollNo(int rollNo){
        this.rollNo = rollNo ;
    }

    public int getRollNo(){
        return rollNo;
    }

    public void setName (String name){
        this.name = name ;
    }

    public String getName (){
        return name 
    }

    public void setMarks (int marks){
        this.marks = marks ;
    }

    public int getMarks (){
        return marks ; 
    }
}

public class School {
    public static void main(String[] args){
        Student obj = new Student () ;
        
        obj.setRollNo(3);
        obj.setName("Bhuvan");
        obj.setMarks(95);

        System.out.println("roll no is : "+ obj.getRollNo());
        System.out.println("Name is : "+obj.getName());
        System.out.println("Marks is : "+obj.getMarks());
    }
}

```


## Inheritance 

    * This is a method in which one class is allowed to inherit the properties of other class .

    * The class which is inherited is called as Parent class or super class .

    * The class which is inheriting is called as Sub class .


### Inheritance code sample .

```
public class Human{
    private int age ; 
    private String name ;

    public void setAge(age){
        this.age = age ;
    }

    public int getAge(){
        return age ;
    }

    public void setName(String name){
        this.nem = name ;
    }

    public String getName (){
        return name 
    }
}

public class Student extends Human {       // extends from Human parent class 

    private int marks ;

    public void setMarks(int marks){
        this.marks = marks ;
    }

    public int getMarks(){
        return marks ;
    }

}

public class School {
    public static void main (String[] args){
        
        Student obj = new Student () ;

         obj.setAge(22) ;         // even this can be accessed as the setAge property can be accessed in student class because it extends Human class ;
         obj.setName("Bhuvan") ;
         obj.setMarks(90) ;

         System.out.println("age: "+ obj.getAge());
         System.out.println("name : "+ obj.getName());
         System.out.println("marks : "+ obj.getMarks());
    }
}

```

## Abstract class ;
    
    * It is a class in which the methods can be **Declared** as it should be accessed mandotorily .

    * A class which has even one abstract method is called as abstract class .


### code sample for adstraction 

```
public abstract class Human{
    private int age ; 
    private String name ;

    public void setAge(age){
        this.age = age ;
    }

    public int getAge(){
        return age ;
    }

    public void setName(String name){
        this.nem = name ;
    }

    public String getName (){
        return name 
    }

    public abstract void getStatus() ; // this will force the student class to implement this method 

    
}

public class Student extends Human {       // extends from Human parent class 

    private int marks ;

    public void setMarks(int marks){
        this.marks = marks ;
    }

    public int getMarks(){
        return marks ;
    }

    public String getStatus(){
        System.out.println("Learning");
    }

}

public class School {
    public static void main (String[] args){
        
        Student obj = new Student () ;

         obj.setAge(22) ;         // even this can be accessed as the setAge property can be accessed in student class because it extends Human class ;
         obj.setName("Bhuvan") ;
         obj.setMarks(90) ;

         System.out.println("age: "+ obj.getAge());
         System.out.println("name : "+ obj.getName());
         System.out.println("marks : "+ obj.getMarks());
         system.out.println(obj.getStatus()) ; // this will call the abstact class 
    }
}

```

## Polymorphism 

    * Poly means **many** , it allows us to declare methods inside the particular class with the same name but different parameters .

    * It uses the methods from other class to perform various task .


### code sample for Polymorphism 

```
class Human{
    public void getStatus(){
        system.out.println("Everyone is in some state")
    }
}

class Student extends Human {
    public void getStatus(){
        System.out.println("Students are learning")
    }
}

class Teacher extends Human {
    public void getStatus(){
        System.out.println("Teachers are teaching ")
    }
}

class School {
    public static void main(String[] args){
        Human obj = new Human() ;
        Human obj1 = new Student() ;
        Human obj2 = new Teacher () ;

        obj.getStatus() ;
        obj1.getStatus();
        obj2.getStatus();
    }
}

```

To learn more about OOP's Refer -- [Java t point](https://www.javatpoint.com/java-oops-concepts) .